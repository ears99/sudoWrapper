#!/bin/sh 
# check to see if this script is running as root, if not, use "su" to re-launch itself.
# $0 is a handy way of making a script refer to itself, and the whoami command can tell us who we are (are we root?)
[ 'whoami' = root ] || exec su -c $0 root 
ls /root

# Note the use of exec. It means "replace this program by", which effectively ends its execution and starts the new program, 
# launched by su, with root, to run from the top. 
# The replacement instance is "root" so it doesn't execute the right side of the ||
